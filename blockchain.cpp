#include "blockchain.h"
#include <iostream>
using namespace std;

blockchain::blockchain()
{
    chain.emplace_back(block(0, "Genesis Block"));
    difficulty = 2;
}

block blockchain::tail() const
{
    return chain.back();
}

void blockchain::add(block newKid)
{
    newKid.prevHash = tail().getHash();
    newKid.mine(difficulty);
    chain.push_back(newKid);
    cout << "loop ";
}
