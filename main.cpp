#include "blockchain.h"
#include "voter.h"
#include <string>
#include <iostream>

using namespace std;

int main()
{
    blockchain bc = blockchain();
    voter billy = voter("William","Richard","Romansky",15,12,1998,"Democrat",true,true,true,"BERNIEANDKANYE2020");

    string first, mid, last; // Strings for voter name
    int d,m,y; // Ints for the voter dob
    string pol; // Political part affiliation
    bool question1, question2, question3; // Simple law passing polls
    string pres; // Presidential election string
    char yesNo;
    int ind = 1;
    do
    {
        cout << "2018 Blockchain Election" << endl;
        cout << "Enter first name: ";
        cin >> first;
        cout << "Enter middle name: ";
        cin >> mid;
        cout << "Enter last name: ";
        cin >> last;

        cout << "What political party are you affiliated with? ";
        cin >> pol;

        cout << "Do you think law #1 should be passed?" << endl;
        cout << "(y/n) ";
        cin >> yesNo;
        if(yesNo == 'y'){
            question1 = true;
        } else {
            question1 = false;
        }

        cout << "Do you think law #2 should be passed?" << endl;
        cout << "(y/n) ";
        cin >> yesNo;
        if(yesNo == 'y'){
            question2 = true;
        } else {
            question2 = false;
        }

        cout << "Do you think law #3 should be passed?" << endl;
        cout << "(y/n) ";
        cin >> yesNo;
        if(yesNo == 'y'){
            question3 = true;
        } else {
            question3 = false;
        }

        cout << "Who is your choice for President? ";
        cin >> pres;

        voter v = voter(first,mid,last,d,m,y,pol,question1,question2,question3,pres);
        string vString = v.toString();

        cout << "Mining voter..." << endl;

        block vBlock = block(ind,vString);
        bc.add(vBlock);

        cout << "Enter another voter?" << endl;
        cout << "(y/n) ";
        cin >> yesNo;
        if(yesNo == 'n') {
            break;
        }

    } while(true);

    return 0;
}
