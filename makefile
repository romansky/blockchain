.PHONY: all

GCC = g++
CFLAGS = -std=c++17 -Wall -O3
LDFLAGS = -I.
OBJDIR = obj

CLASSES = blockchain block voter
OBJECTS = $(addsuffix .o, $(CLASSES))
OBJFILES = $(addprefix $(OBJDIR)/, $(OBJECTS))

all: main

main: $(OBJDIR)/main.o $(OBJFILES)
	$(GCC) -o main.out $(CFLAGS) $(OBJDIR)/main.o $(OBJFILES) $(LDFLAGS)

$(OBJDIR)/main.o: main.cpp
	$(GCC) -c -g $(CFLAGS) main.cpp -o $(OBJDIR)/main.o

.SECONDEXPANSION:
$(OBJFILES): %.o: $$(notdir %.cpp %.h)
	$(GCC) -c -g $(CFLAGS) $< -o $@

clean:
	rm -rf $(OBJDIR)/*.o
	rm main.out
