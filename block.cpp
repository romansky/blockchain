#include "block.h"
#include <functional>
#include <iostream>
#include <sstream>
#include <utility>
#include <iomanip>

using namespace std;

block::block(unsigned int i, const string &d) // i for incoming index and d for incoming data
{
    index = i;
    data = d;
    nonce = -1;
    ttime = time(nullptr);
}

string block::getHash()
{
    return bHash;
}

void block::mine(unsigned int diff)
{
    char cstr[diff+1];
    
    for(unsigned int i=0; i<diff; i++)//creating an array of characters of difficulties+1
    {
        cstr[i]='0';
    }
    
    cstr[diff]='\0';
    
    string str(cstr);
    
    // I can't tell if this is an infinite loop or it just takes
    // a long time to mine
    do{
        nonce++;
        bHash=setHash();
        // cout << "line40 ";
    }while (bHash.substr(0,diff)!=str);
    
    cout << "The block has been mined "<< bHash << endl;
}

string block::setHash() const
{
    //string ret;
    //ret.append(to_string(index) + to_string(ttime) + to_string(data) + to_string(nonce) + to_string(prevHash));
    // NOTE: Learned that the string.append function does not
    // allow for multiple data types but stringstream does
    
    // I realized in this function there is A LOT of messing with
    // different data types such as converting size_t to string
    
    stringstream preHash;
    preHash << index << ttime << data << nonce << prevHash;
    string toHash = preHash.str();
    hash<string> hash_func;
    size_t hash = hash_func(toHash);
    stringstream postHash;
    postHash << hash;

    return postHash.str();
}
