# Simple Blockchain Implementation in C++

Dependencies: C++ Standard Library
How to build: Run $make with no arguments

## Classes

### Block
The block object contains an index, nonce (for mining purposes),
a string for data, a hash of itself, its previous,and transaction
time.

The mine function is the most complicated part of the data
structure. The function first creates a string of 0s for the
difficulty of the mining operation. Until the hash function
returns a string equal to the nonce and hash concatenated.
Once the two match the block is added to the end of the chain.

### Blockchain
The blockchain object is simply a collection of blocks that allows
the user to add a block to the tail end. The add function calls
the mine function of the block object.

### Voter
The voter object is a class and not a struct because of the need of
private member variables. The object contains a voter's first,
middle, and last name, political party affiliation, answers to poll
questions, and write-in candidate field.
