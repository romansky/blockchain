#ifndef BLOCKCHAIN_H
#define BLOCKCHAIN_H

#include "block.h"
#include <vector>

class blockchain
{
    private:
        unsigned int difficulty;
        std::vector<block> chain;
        block tail() const;

    public:
        blockchain();
        void add(block newKid); // on the block chain hahahahahaha get it

};
#endif
