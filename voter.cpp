#include "voter.h"

using namespace std;

string voter::toString()
{
    stringstream ss;
    ss << fName << mName << lName << day << month << year << party << q1 << q2 << q3 << candidate;

    return ss.str();
}
