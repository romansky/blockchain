#ifndef BLOCK_H
#define BLOCK_H

#include <ctime> // when we call the current time of the function
#include <string> //for string functions

// using namespace std;

class block
{
    private:
        unsigned int index;
        unsigned int nonce;
        std::string data;
        std::string bHash;
        time_t ttime;

        std::string setHash() const;
    public:
        block(unsigned int i, const std::string &d); // Constructor
        std::string prevHash; // Though it was interesting that the previous has node is a public member variable
        std::string getHash();
        void mine(unsigned int diff);
        
};
#endif
