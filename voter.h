#ifndef VOTER_H
#define VOTER_H

#include <string>
#include <sstream>

class voter
{
    private:
        std::string fName, mName, lName;
        int day,month,year;
        std::string party;
        bool q1,q2,q3;
        std::string candidate;    
    public:
        voter(std::string f, std::string m, std::string l, int cDay, int cMonth, int cYear, std::string p, bool cQ1, bool cQ2, bool cQ3, std::string can)
        {
            fName = f;
            mName = m;
            lName = l;
            day = cDay;
            month = cMonth;
            year = cYear;
            party = p;
            q1 = cQ1;
            q2 = cQ2;
            q3 = cQ3;
            candidate = can;
        }
        std::string toString();
};

#endif
